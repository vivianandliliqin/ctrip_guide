# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class CtripGuideItem(scrapy.Item):

    title = scrapy.Field()
    path = scrapy.Field()
    image_urls = scrapy.Field()
    score = scrapy.Field()
    price = scrapy.Field()
    contact = scrapy.Field()
    name = scrapy.Field()
    head_image = scrapy.Field()
    tag = scrapy.Field()
    gender = scrapy.Field()
    service_zone = scrapy.Field()
    zone_ids = scrapy.Field()
    service_type = scrapy.Field()
    service_content = scrapy.Field()
    service_tips = scrapy.Field()
    service_time = scrapy.Field()
    comments = scrapy.Field()
    source_url = scrapy.Field()
    date_time = scrapy.Field()

#
# soup = BeautifulSoup(te)
# new_content = ''
# for c in soup.body.div.children:
#     if isinstance(c, Tag):
#         if c.name == 'p' and c.get('class') == None:
#             print c
#             print 'c.string',c.string
#             new_content += c.string
#         elif c.name == 'br':
#             new_content += c.prettify()
#     elif c:
#         new_content += c
#
# if new_content:
#     p_tag = soup.new_tag("p", **{'class': 'content'})
#     p_tag.string = new_content
#     res = p_tag.prettify(formatter=None)
#     for r in repla:
#         res = res.replace(r, '')

