# -*- coding: utf-8 -*-
from scrapy.exceptions import IgnoreRequest
from ctrip_guide import utils as mu
import random
import logging
logging.basicConfig(level=logging.DEBUG,
    format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
    datefmt='%a, %d %b %Y %H:%M:%S')
logger = logging.getLogger(__name__)


class DedupMiddleware(object):
    def __init__(self):
        self.db = mu.get_mongo_client()

    def process_request(self, request, spider):
        url = request.url
        mafengwo = self.db.tour_service_url.find_one({"source": url})
        # print("()()((mafengwo>>>>>>>>%s" % mafengwo)
        if mafengwo:
            logger.info('ignore duplicated url: <%s>' % url)

            # 此处需要抛出异常，否则会继续执行
            raise IgnoreRequest()


class RandomUserAgent(object):
    """
    Randomly rotate user agents based on a list of predefined ones
    """
    def __init__(self, pc, phone):
        self.pc = pc
        self.phone = phone
        # self.cookie = "__cfduid=dc59c0c3fcc174831e6196bd32e19e3511476324187; PHPSESSID=1jkai9e9rrlu8fe82p7acci5k7; _ga=GA1.2.1088161425.1476324190; _dc_gtm_UA-1742617-1=1; _vwo_uuid_v2=DFD02950C3EAB548CB243C3668C80682|2d00a9822b58ad920fc9c12b225448e4; __atuvc=1%7C41; __atuvs=57feeb5d0fd6d3d4000; wp_woocommerce_session_ba40aa3b257af5e64b4801970f6577cb=ba425f502711826011e9ba96a4a4d816%7C%7C1476496987%7C%7C1476493387%7C%7C05235a1e7166b7dad42a070b37511688"

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.settings.getlist('PC_USER_AGENTS'), crawler.settings.getlist('PHONE_USER_AGENTS'))

    def process_request(self, request, spider):
        agents = self.pc
        # url = request.url
        # qunar = 'qunar.com'
        # if qunar in url:
        #     agents = self.pc
        request.headers.setdefault('User-Agent', random.choice(agents))
        # request.headers.setdefault('Cookie', self.cookie)
