# -*- coding: utf-8 -*-
from ctrip_guide import utils as mu


class CtripGuidePipeline(object):
    def __init__(self):
        self.db = mu.get_mongo_client()

    def process_item(self, item, spider):
        data = dict(item)
        self.db.tour_service.insert_one(data)
        return item


class DedupPipeline(object):
    """
    存放的时候 记录一下
    """
    def __init__(self):
        self.db = mu.get_mongo_client()

    def process_item(self, item, spider):
        url = item['source_url']
        self.db.tour_service_url.insert_one({"source": url})
        return None
