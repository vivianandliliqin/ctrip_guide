# -*- coding: utf-8 -*-
import scrapy
import base64
import os
import re
import datetime
import urlparse
from ctrip_guide.items import CtripGuideItem
from bs4 import BeautifulSoup
from bs4.element import Tag


class ToureGuideSpider(scrapy.Spider):
    name = "toure_guide"
    allowed_domains = ["ctrip.com"]
    start_urls = (
        'http://you.ctrip.com/place',
    )

    def __init__(self):
        # self.db = mu.get_mongo_client()
        self.host = 'http://you.ctrip.com'
        super(ToureGuideSpider, self).__init__()

    def parse(self, response):
        dls = response.xpath('//div[@class="side"]//dl')
        dls.pop(0)
        china_city = dls.pop(0)

        for ch in china_city.xpath('.//li/a/@href').extract():
            ch = ch.replace("/place/", '/dangdiren/')
            ci_url = urlparse.urljoin(self.host, ch)
            yield scrapy.Request(ci_url, callback=self.parse_guide_list)
        country_paths = []
        for d in dls:
            pat = d.xpath('.//li/a/@href').extract()
            country_paths += pat
        for co in country_paths:
            co = co.replace("/place/", '/dangdiren/')
            co_url = urlparse.urljoin(self.host, co)
            yield scrapy.Request(co_url, callback=self.parse_country)

    def parse_country(self, response):
        city_list = response.xpath("//ul[@class='country_list']//li/a/@href").extract()
        for city in city_list:
            url = urlparse.urljoin(self.host, city)
            yield scrapy.Request(url, callback=self.parse_guide_list)

    def parse_guide_list(self, response):
        guide_list = response.xpath('//a[@class="ddr_service_box"]/@href').extract()
        npage = response.xpath('//a[contains(@class, "nextpage")]/@href').extract_first()
        if npage and "javascript" not in npage:
            next_page = urlparse.urljoin(self.host, npage)
            yield scrapy.Request(next_page, callback=self.parse_guide_list)
        for guide in guide_list:
            url = urlparse.urljoin(self.host, guide)
            yield scrapy.Request(url, callback=self.parse_guide)

    def parse_content(self, content):
        res = None
        if not content:
            return res
        te = content.pop()
        repla = [u"请注明在携程上看到的！", u"携程"]
        soup = BeautifulSoup(te)
        new_content = ''
        for c in soup.body.div.children:
            if isinstance(c, Tag):
                if c.name == 'p' and c.get('class') is None:
                    if c.string:
                        new_content += c.string
                elif c.name == 'br':
                    new_content += c.prettify()
            elif c:
                new_content += c

        if new_content:
            p_tag = soup.new_tag("p", **{'class': 'content'})
            p_tag.string = new_content
            res = p_tag.prettify(formatter=None)
            for r in repla:
                res = res.replace(r, '')
        return res

    def parse_guide(self, response):
        item = CtripGuideItem()

        item['title'] = response.xpath('//div[@class="title cf"]/h1/text()').extract_first()
        nav = response.xpath('//div[@class="breadbar_v1 cf"]/ul/li/a/text()').extract()
        item['path'] = nav[2:-1]
        item['image_urls'] = response.xpath('//ul[@class="service_slider"]/li/img/@src').extract()
        item['score'] = response.xpath('//span[@class="score"]/b/text()').extract_first()
        price_num = response.xpath('//p[@class="s_price"]/span/b/text()').extract_first()
        price_des = response.xpath('//p[@class="s_price"]/span/text()').extract_first()
        item['price'] = price_num and price_num + price_des or price_des
        codestr = response.xpath('//div[@class="service_intro_r"]//input/@value').extract_first()
        item['contact'] = base64.decodestring(codestr).replace(' ', '')
        servicer = response.xpath('//a[@class="servicer_headimg"]')
        item['head_image'] = servicer.xpath('./img/@src').extract_first()
        get_gender = servicer.xpath('./i/@class').extract()
        item['gender'] = get_gender.pop()[5:] == 'male' and '男' or '女'
        item['name'] = response.xpath('//p[@class="servicer_name"]/text()').extract_first()
        item['tag'] = response.xpath('//div[@class="servicer_icon"]/span/text()').extract()
        item['service_type'] = response.xpath('//p[@class="service_type"]/text()').extract_first()
        item['service_zone'] = response.xpath('//p[@class="service_type"]/a/text()').extract()
        texts = response.xpath('//div[@class="service_text"]').extract()
        item['service_content'] = self.parse_content(texts)
        item['service_tips'] = self.parse_content(texts)
        item['service_time'] = self.parse_content(texts)
        item['date_time'] = datetime.datetime.utcnow()
        item['source_url'] = response.url
        return item