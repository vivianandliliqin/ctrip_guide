# -*- coding: utf-8 -*-
import sys
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure


def get_mongo_client():
    """
    Connect to MongoDB
    """
    try:
        # c = MongoClient("mongodb://upenergy:upenergy@db0.silknets.com:27000/sso")
        uri = "mongodb://127.0.0.1:27017/"
        # uri = u"mongodb://mongo:mongo@127.0.0.1/sso?authMechanism=SCRAM-SHA-1"
        c = MongoClient(uri)
    except ConnectionFailure, e:
        sys.stderr.write("Could not connect to MongoDB: %s" % e)
        sys.exit(1)

    # Get a Database handle to a database named "mydb"
    dbh = c["parse_work"]
    return dbh